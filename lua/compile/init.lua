local M = {}
local compile_com = package.loaded._G.compile_command

M.set_compile_command = function(command)
  compile_com = command
end

M.run_compile_command = function(com)
  if (string.sub(com, 1, 1) ~= "!") then
    com = "!" .. com
  end
  vim.api.nvim_command(com)
end

-- M.set_compile_command("!python %")
-- M.run_compile_command(compile_com)

print(compile_com)

return M
